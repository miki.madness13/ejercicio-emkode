import { Component } from '@angular/core';
import { Employee } from './models/employee';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  employeeArray: Employee[] = [
  { "id": 1, "nombre": "Allan", "apellido": "Poe", "email": "allan_poe_09@email.com", "telefono": "4435679083", "acciones": "ok" },
    { "id": 2, "nombre": "Clive S.", "apellido": "Lewis", "email": "cs_lewis_98@email.com", "telefono": "4437094524", "acciones": "ok"  },
    { "id": 3, "nombre": "JK", "apellido": "Rowling", "email": "jk_rowling_65@email.com", "telefono": "4436122456", "acciones": "ok"  },
    { "id": 4, "nombre": "Cassandra", "apellido": "Clare", "email": "cassandra_clare_73@email.com", "telefono": "4437972345", "acciones": "ok"  },
    { "id": 5, "nombre": "Stephen", "apellido": "King", "email": "stephen_king_47@email.com", "telefono": "4438996523", "acciones": "ok"  }
  ];

selectedEmployee: Employee = new Employee();

openForEdit(employee: Employee){
	this.selectedEmployee = employee;
	
}

addOrEdit(){
	if(this.selectedEmployee.id === 0){
	this.selectedEmployee.id=this.employeeArray.length+1;
	this.employeeArray.push(this.selectedEmployee);	
	}	

	this.selectedEmployee = new Employee;
}

delete(){
	if(confirm('Seguro que quieres eliminar?')){
	this.employeeArray = this.employeeArray.filter(x => x != this.selectedEmployee);
	this.selectedEmployee = new Employee();
	}
}

}
